#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

void *doWork( void *params );

const int N = 100;
const int max = 10;
int counter = 0;

pthread_mutex_t counter_lock;

int main() {
    pthread_t threads[N];
//    timespec delay {0, 100000000};
//    timespec rest {0, 0};

    pthread_mutex_init(&counter_lock, NULL);

    for(int k=0;k<5;k++) {
        counter = 0;
        for (int i = 0; i < N; i++) {
            pthread_create(threads + i, 0, doWork, 0);
        }

        for (int i = 0; i < N; i++) {
            pthread_join(threads[i], 0);
        }

        std::cout << counter << std::endl;
        //nanosleep(&delay, &rest);
    }

    return 0;
}

void *doWork( void *params )
{
    timespec delay {0, 10};
    timespec rest {0, 0};

    for(int i=0; i<max*2; i++) {
       pthread_mutex_lock(&counter_lock);
        if(counter < max) {
            nanosleep(&delay, &rest);
            counter++;
        }
        pthread_mutex_unlock(&counter_lock);
    }
}