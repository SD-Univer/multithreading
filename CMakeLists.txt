cmake_minimum_required(VERSION 3.14)
project(mt_1)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_BUILD_TYPE Release)
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

add_executable(mt_1 main.cpp)

target_link_libraries(mt_1 pthread)